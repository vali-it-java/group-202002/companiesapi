package com.mareklints.companiesapi.controller;

import com.mareklints.companiesapi.model.Company;
import com.mareklints.companiesapi.repository.CompanyRepository;
import com.mareklints.companiesapi.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyService companyService;

    @GetMapping
    public List<Company> getCompanies() {
        return companyRepository.fetchAllCompanies();
    }

    @GetMapping("/{companyId}")
    public Company getSingleCompany(@PathVariable int companyId) {
        return companyRepository.fetchSingleCompany(companyId);
    }

    @DeleteMapping("/{companyId}")
    public void deleteSingleCompany(@PathVariable int companyId) {
        companyRepository.removeSingleCompany(companyId);
    }

    @PostMapping
    public void editCompany(@RequestBody Company company) {
        companyService.editCompany(company);
    }
}
