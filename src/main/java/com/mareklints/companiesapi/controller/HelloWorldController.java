package com.mareklints.companiesapi.controller;

import com.mareklints.companiesapi.model.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/HelloWorld")
public class HelloWorldController {

    @GetMapping("/hello1")
    public String hello1() {
        return "Hello, World (vol 1)";
    }

    @GetMapping("/hello2/{userName}")
    public String hello2(@PathVariable String userName) {
        return "Hello, " + userName + "!";
    }

    @GetMapping("/person")
    public Person getPerson() {
        Person teet = new Person("Teet", "Kask", 5000);
        return teet;
    }

    @GetMapping("/persons")
    public Person[] getRichCustomers() {
        Person teet = new Person("Teet", "Kask", 5000);
        Person malle = new Person("Malle", "Tamm", 7000);
        Person mai = new Person("Mai", "Vaher", 8000);
        Person[] persons = {
                teet, malle, mai
        };
        return persons;
    }
}
