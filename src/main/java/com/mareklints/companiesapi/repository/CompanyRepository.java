package com.mareklints.companiesapi.repository;

import com.mareklints.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Company> fetchAllCompanies() {
        return jdbcTemplate.query(
                "select * from company",
                (dbRow, sequenceNumber) -> {
                    return new Company(
                            dbRow.getInt("id"),
                            dbRow.getString("name"),
                            dbRow.getString("logo"));
                }
        );
    }

    public Company fetchSingleCompany(int companyId) {
        List<Company> companies = jdbcTemplate.query(
                "select * from company where id = ?",
                new Object[]{companyId},
                (dbRow, sequenceNumber) -> {
                    return new Company(
                            dbRow.getInt("id"),
                            dbRow.getString("name"),
                            dbRow.getString("logo"));
                }
        );
        if (companies.size() > 0) {
            return companies.get(0);
        } else {
            return null;
        }
    }

    public void removeSingleCompany(int companyId) {
        // Päringunäidis: delete from company where id = 3
        jdbcTemplate.update("delete from company where id = ?", companyId);
    }

    public void addSingleCompany(Company company) {
        jdbcTemplate.update("insert into company (`name`, `logo`) values (?, ?)",
                company.getName(), company.getLogo());
    }

    public void updateSingleCompany(Company company) {
        // update [table] set [column1] = [value1], [column2] = [value2] where [condition]
        jdbcTemplate.update("update company set `name` = ?, `logo` = ? where `id` = ?",
                company.getName(), company.getLogo(), company.getId());
    }
}
