package com.mareklints.companiesapi.service;

import com.mareklints.companiesapi.model.Company;
import com.mareklints.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public void editCompany(Company company) {
        if (company.getId() > 0){
            // Olemasoleva ettevõtte muutmine
            companyRepository.updateSingleCompany(company);
        } else {
            // Uue ettevõtte lisamine
            companyRepository.addSingleCompany(company);
        }
    }

}
